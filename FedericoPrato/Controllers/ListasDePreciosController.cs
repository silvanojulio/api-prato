﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FedericoPrato.Controllers
{
    public class ListasDePreciosController: ApiController
    {
        public List<LisitaDePrecioModel> Get(int id)
        {
            using (var db = new FedericoPratoDb())
            {
                var clientes = db.Database.SqlQuery<LisitaDePrecioModel>(
                    " SELECT TOP 500 " +
                    " GVA17.ID_GVA17, GVA17.COD_ARTICU, GVA17.PRECIO, " +
                    " GVA17.NRO_DE_LIS, STA11.PERFIL, STA11.DESCRIPCIO, " +
                    " STA11.DESC_ADIC, STA11.SINONIMO, STA11.OBSERVACIONES, " +
                    " STA19.COD_DEPOSI, STA19.CANT_STOCK, CPA15.COD_PROVEE " +
                    " FROM GVA17 " +
                    " INNER JOIN STA11 ON(GVA17.COD_ARTICU = STA11.COD_ARTICU) " +
                    " INNER JOIN STA19 ON(GVA17.COD_ARTICU = STA19.COD_ARTICU) " +
                    " INNER JOIN CPA15 ON(GVA17.COD_ARTICU = CPA15.COD_ARTICU) " +
                    " WHERE STA19.COD_DEPOSI = '3' " +
                    " AND STA19.CANT_STOCK >= 0 AND STA11.PERFIL = 'A' AND GVA17.NRO_DE_LIS BETWEEN 2 AND 9 " +
                    " AND GVA17.ID_GVA17 > {0}" +
                    " ORDER BY GVA17.ID_GVA17 ASC", id);

                return clientes.ToList();
            }
        }
        
    }

    public class LisitaDePrecioModel
    {
        public int ID_GVA17 { get; set; }
        public string COD_ARTICU { get; set; }
        public Nullable<decimal> PRECIO { get; set; }

        public short NRO_DE_LIS { get; set; }
        public string PERFIL { get; set; }
        public string DESCRIPCIO { get; set; }

        public string DESC_ADIC { get; set; }
        public string SINONIMO { get; set; }
        public string OBSERVACIONES { get; set; }

        public string COD_DEPOSI { get; set; }
        public Nullable<decimal> CANT_STOCK { get; set; }
        public string COD_PROVEE { get; set; }
    }
}
