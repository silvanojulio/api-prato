﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FedericoPrato.Controllers
{
    public class ListasDePreciosMinoristaController : ApiController
    {
        public List<LisitaDePrecioModel> Get(int id)
        {
            using (var db = new FedericoPratoDb())
            {
                var items = db.Database.SqlQuery<LisitaDePrecioModel>(
                    " SELECT TOP 500 " +
                    " GVA17.ID_GVA17, GVA17.COD_ARTICU, GVA17.PRECIO, " +
                    " GVA17.NRO_DE_LIS, STA11.PERFIL, STA11.DESCRIPCIO, " +
                    " STA11.DESC_ADIC, STA11.SINONIMO, STA11.OBSERVACIONES, " +
                    " STA19.COD_DEPOSI, STA19.CANT_STOCK, CPA15.COD_PROVEE " +
                    " FROM GVA17 " +
                    " INNER JOIN STA11 ON(GVA17.COD_ARTICU = STA11.COD_ARTICU) " +
                    " INNER JOIN STA19 ON(GVA17.COD_ARTICU = STA19.COD_ARTICU) " +
                    " INNER JOIN CPA15 ON(GVA17.COD_ARTICU = CPA15.COD_ARTICU) " +
                    " WHERE STA19.COD_DEPOSI = '1' " +
                    " AND STA11.PERFIL = 'A' AND GVA17.NRO_DE_LIS = 1 " +
                    " AND CPA15.COD_PROVEE = 'PSHOP' AND GVA17.ID_GVA17 > {0}" +
                    " ORDER BY GVA17.ID_GVA17 ASC", id);

                return items.ToList();
            }
        }
    }

}
