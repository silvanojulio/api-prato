﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FedericoPrato.Controllers
{
    public class PedidosController : ApiController
    {
        [HttpPost]
        public PedidoGuardadoModel GuardarPedido(PedidoModel modelo)
        {
            using (var db = new FedericoPratoDb())
            {
                var r = new PedidoGuardadoModel();

                var direccion = db.DIRECCION_ENTREGA.FirstOrDefault(x => x.COD_CLIENTE == modelo.COD_CLIENT);

                var cab = NuevoGVA21();

                cab.COD_SUCURS = "3";
                cab.ESTADO = 2;
                cab.N_REMITO = " 000000000000";
                cab.ORIGEN = "T";
                cab.REVISO_FAC = "A";
                cab.REVISO_PRE = "A";
                cab.REVISO_STK = "A";
                cab.HORA = "120000";
                cab.ID_ASIENTO_MODELO_GV = 1;
                cab.HORA_INGRESO = "120000";
                cab.USUARIO_INGRESO = "GERARDOM";
                cab.TERMINAL_INGRESO = "GERARDO";

                cab.COD_CLIENT = modelo.COD_CLIENT;
                cab.COD_TRANSP = modelo.COD_TRANSP;
                cab.COD_VENDED = modelo.COD_VENDED;
                cab.COND_VTA = modelo.COND_VTA;
                cab.FECHA_PEDI = modelo.FECHA_PEDI;
                cab.N_LISTA = modelo.N_LISTA;
                cab.TALON_PED = modelo.TALON_PED;
                cab.TOTAL_PEDI = modelo.TOTAL_PEDI;
                cab.ID_DIRECCION_ENTREGA = direccion != null ? direccion.ID_DIRECCION_ENTREGA : modelo.ID_DIRECCION_ENTREGA;
                
                db.GVA21.Add(cab);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw;
                }
                
                var relleno = "00002";
                while ( (relleno + cab.ID_GVA21).Length < 13) relleno = relleno + "0";

                var nroPedido = relleno + cab.ID_GVA21;
                cab.NRO_PEDIDO = nroPedido;

                try
                {
                    foreach (var item in modelo.Items)
                    {
                        var detalle = NuevoGVA03();
                        detalle.CAN_EQUI_V = 1;
                        detalle.COD_ARTICU = item.COD_ARTICU;
                        detalle.NRO_PEDIDO = nroPedido;
                        detalle.N_RENGLON = item.N_RENGLON;
                        detalle.CANT_PEDID = item.CANT_PEDID;
                        detalle.CANT_A_DES = item.CANT_A_DES;
                        detalle.CANT_A_FAC = item.CANT_A_FAC;
                        detalle.CANT_PEN_D = item.CANT_PEDID;
                        detalle.CANT_PEN_F = item.CANT_PEDID;
                        detalle.PRECIO = item.PRECIO;
                        detalle.TALON_PED = item.TALON_PED;
                        
                        db.GVA03.Add(detalle);
                    }
                
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    db.GVA21.Remove(cab);
                    db.SaveChanges();
                    throw;
                }

                r.NroDePedido = cab.NRO_PEDIDO;

                return r;
            }
        }

        private GVA21 NuevoGVA21()
        {
            var item = new GVA21();
            item.FILLER = "";
            item.APRUEBA = "";
            item.CIRCUITO = 1;
            item.COD_CLIENT = "";
            item.COD_SUCURS = "";
            item.COD_TRANSP = "";
            item.COD_VENDED = "";
            item.COMENTARIO = "";
            item.COMP_STK = false;
            item.COND_VTA = 0;
            item.COTIZ = 1;
            item.ESTADO = 2;
            item.EXPORTADO = false;
            item.FECHA_APRU = new DateTime(1800,1,1);
            item.FECHA_ENTR = new DateTime(1800,1,1);
            item.FECHA_PEDI = new DateTime(1800,1,1);
            item.HORA_APRUE = "";
            item.ID_EXTERNO = "";
            item.LEYENDA_1 = "";
            item.LEYENDA_2 = "";
            item.LEYENDA_3 = "";
            item.LEYENDA_4 = "";
            item.LEYENDA_5 = "";
            item.MON_CTE = true;
            item.N_LISTA = 0;
            item.N_REMITO = "";
            item.NRO_O_COMP = "";
            item.NRO_PEDIDO = "";
            item.NRO_SUCURS = 0;
            item.ORIGEN = "";
            item.PORC_DESC = 0;
            item.REVISO_FAC = "";
            item.REVISO_PRE = "";
            item.REVISO_STK = "";
            item.TALONARIO = 0;
            item.TALON_PED = 0;
            item.TOTAL_PEDI = 0;
            item.TIPO_ASIEN = "";
            item.MOTIVO = "";
            item.HORA = "";
            item.COD_CLASIF = "";
            item.TAL_PE_ORI = 0;
            item.NRO_PE_ORI = "";
            item.FECHA_INGRESO = new DateTime(1800,1,1);
            item.HORA_INGRESO = "";
            item.USUARIO_INGRESO = "";
            item.TERMINAL_INGRESO = "";
            item.FECHA_ULTIMA_MODIFICACION = new DateTime(1800,1,1);
            item.HORA_ULTIMA_MODIFICACION = "";
            item.USUA_ULTIMA_MODIFICACION = "";
            item.TERM_ULTIMA_MODIFICACION = "";
            item.ES_PEDIDO_WEB = false;
            item.WEB_ORDER_ID = 0;
            item.FECHA_O_COMP = new DateTime(1800,1,1);
            item.ACTIVIDAD_COMPROBANTE_AFIP = "";
            item.TIPO_DOCUMENTO_PAGADOR = null;
            item.NUMERO_DOCUMENTO_PAGADOR = "";
            item.NRO_OC_COMP = "";
            item.USUARIO_TIENDA = null;
            item.TIENDA = "e-Commerce";
            item.ORDER_ID_TIENDA = "";
            item.TOTAL_DESC_TIENDA = 0;
            item.TIENDA_QUE_VENDE = "";
            item.PORCEN_DESC_TIENDA = 0;

            return item;
        }

        private GVA03 NuevoGVA03()
        {
            var item = new GVA03
            {
                FILLER = "",
                CAN_EQUI_V = 0,
                CANT_A_DES = 0,
                CANT_A_FAC = 0,
                CANT_PEDID = 0,
                CANT_PEN_D = 0,
                CANT_PEN_F = 0,
                COD_ARTICU = "",
                DESCUENTO = 0,
                NRO_PEDIDO = "",
                PEN_REM_FC = 0,
                PEN_FAC_RE = 0,
                PRECIO = 0,
                TALON_PED = 0,
                COD_CLASIF = "",
                PRECIO_LISTA = 0,
                PRECIO_BONIF = 0,
                DESCUENTO_PARAM = 0,
                PRECIO_FECHA = null,
                FECHA_MODIFICACION_PRECIO = null,
                USUARIO_MODIFICACION_PRECIO = null,
                TERMINAL_MODIFICACION_PRECIO = null,
                CANT_A_DES_2 = 0,
                CANT_A_FAC_2 = 0,
                CANT_PEDID_2 = 0,
                CANT_PEN_D_2 = 0,
                CANT_PEN_F_2 = 0,
                PEN_REM_FC_2 = 0,
                PEN_FAC_RE_2 = 0,
                UNIDAD_MEDIDA_SELECCIONADA = "V",
                COD_ARTICU_KIT = "",
                PROMOCION = false,
                PRECIO_ADICIONAL_KIT = 0,
                KIT_COMPLETO = false,
                INSUMO_KIT_SEPARADO = false,
                ID_MEDIDA_VENTAS = 4,
                ID_MEDIDA_STOCK = 4,
                RENGL_PADR = 0,
                
            };

            return item;
        }
    }

    public class PedidoModel
    {
        public string COD_CLIENT { get; set; }
        public string COD_TRANSP { get; set; }
        public string COD_VENDED { get; set; }
        public Nullable<short> COND_VTA { get; set; }
        public Nullable<System.DateTime> FECHA_PEDI { get; set; }
        public Nullable<short> N_LISTA { get; set; }
        public short TALON_PED { get; set; }
        public Nullable<decimal> TOTAL_PEDI { get; set; }
        public Nullable<int> ID_DIRECCION_ENTREGA { get; set; }

        public List<ItemPedidoModel> Items { get; set; }

    }

    public class ItemPedidoModel
    {
        public string COD_ARTICU { get; set; }
        public string NRO_PEDIDO { get; set; }
        public Nullable<int> N_RENGLON { get; set; }
        public Nullable<decimal> CANT_PEDID { get; set; }
        public Nullable<decimal> CANT_A_DES { get; set; }
        public Nullable<decimal> CANT_A_FAC { get; set; }
        public Nullable<decimal> PRECIO { get; set; }
        public short TALON_PED { get; set; }

    }

    public class PedidoGuardadoModel
    {
        public string NroDePedido { get; set; }
    }
}
