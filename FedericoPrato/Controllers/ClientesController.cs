﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FedericoPrato.Controllers
{
    public class ClientesController : ApiController
    {
        public List<ClienteModel> Get(int id)
        {
            using (var db = new FedericoPratoDb())
            {
                var clientes = db.Database.SqlQuery<ClienteModel>(
                " SELECT TOP 500 "+
                " ID_GVA14, C_POSTAL, COD_CLIENT, COD_PROVIN, COD_TRANSP, COD_VENDED, COD_ZONA,  " +
                " Cond_Vta,  DOMICILIO, E_MAIL, FECHA_INHA, LOCALIDAD, NRO_LISTA, OBSERVACIO, RAZON_SOCI " +
                " FROM GVA14 WHERE NRO_LISTA BETWEEN 2 AND 9 AND  ID_GVA14 > {0}" +
                " ORDER BY ID_GVA14 ASC", id);

                return clientes.ToList();
            }
        }
    }

    public class ClienteModel
    {
        public int ID_GVA14 { get; set; }
        public string C_POSTAL { get; set; }
        public string COD_CLIENT { get; set; }
        public string COD_PROVIN { get; set; }
        public string COD_TRANSP { get; set; }
        public string COD_VENDED { get; set; }
        public string COD_ZONA { get; set; }
        public Nullable<short> COND_VTA { get; set; }
        public string DOMICILIO { get; set; }
        public string E_MAIL { get; set; }
        public Nullable<System.DateTime> FECHA_INHA { get; set; }
        public string LOCALIDAD { get; set; }
        public Nullable<short> NRO_LISTA { get; set; }
        public string OBSERVACIO { get; set; }
        public string RAZON_SOCI { get; set; }
    }
}
