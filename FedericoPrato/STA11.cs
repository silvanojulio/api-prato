//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FedericoPrato
{
    using System;
    using System.Collections.Generic;
    
    public partial class STA11
    {
        public string FILLER { get; set; }
        public string ADJUNTO { get; set; }
        public Nullable<short> ALI_NO_CAT { get; set; }
        public string BAJA_STK { get; set; }
        public string BMP { get; set; }
        public string CL_SIAP_CP { get; set; }
        public string CL_SIAP_GV { get; set; }
        public Nullable<short> COD_ACTIVI { get; set; }
        public string COD_ARTICU { get; set; }
        public string COD_BARRA { get; set; }
        public string COD_DEP { get; set; }
        public Nullable<short> COD_IB { get; set; }
        public Nullable<short> COD_IB3 { get; set; }
        public Nullable<short> COD_II { get; set; }
        public Nullable<short> COD_II_CO { get; set; }
        public Nullable<short> COD_IVA { get; set; }
        public Nullable<short> COD_IVA_CO { get; set; }
        public Nullable<short> COD_S_II { get; set; }
        public Nullable<short> COD_S_II_C { get; set; }
        public Nullable<short> COD_S_IVA { get; set; }
        public Nullable<short> COD_S_IV_C { get; set; }
        public Nullable<decimal> COMISION_V { get; set; }
        public Nullable<bool> CONSID_TMP { get; set; }
        public Nullable<double> CTA_COMPRA { get; set; }
        public Nullable<double> CTA_VENTAS { get; set; }
        public string CTO_COMPRA { get; set; }
        public string CTO_VENTAS { get; set; }
        public string DESC_ADIC { get; set; }
        public string DESCRIPCIO { get; set; }
        public Nullable<decimal> DESCUENTO { get; set; }
        public Nullable<bool> DESTI_ART { get; set; }
        public Nullable<decimal> EQUIVALE_V { get; set; }
        public string ESCALA_1 { get; set; }
        public string ESCALA_2 { get; set; }
        public string ESPEC_AUTO { get; set; }
        public Nullable<bool> FACT_IMPOR { get; set; }
        public Nullable<bool> FAVORITO { get; set; }
        public Nullable<System.DateTime> FECHA_ALTA { get; set; }
        public Nullable<bool> GEN_IB { get; set; }
        public Nullable<bool> GEN_IB3 { get; set; }
        public Nullable<bool> IMPR_CARTA { get; set; }
        public Nullable<decimal> IMPUESTO_I { get; set; }
        public Nullable<decimal> IMPUEST_IC { get; set; }
        public string MOD_DESCAR { get; set; }
        public Nullable<bool> PERC_NO_CA { get; set; }
        public string PERFIL { get; set; }
        public Nullable<decimal> PORC_DESVI { get; set; }
        public Nullable<decimal> PORC_SCRAP { get; set; }
        public Nullable<decimal> PORC_UTILI { get; set; }
        public Nullable<System.DateTime> PROMODESDE { get; set; }
        public Nullable<System.DateTime> PROMOHASTA { get; set; }
        public string PROMO_MENU { get; set; }
        public Nullable<decimal> PTO_PEDIDO { get; set; }
        public Nullable<double> PUNTAJE { get; set; }
        public Nullable<bool> RET_RNI { get; set; }
        public Nullable<short> RET_RNI_CO { get; set; }
        public string SINONIMO { get; set; }
        public Nullable<bool> STOCK { get; set; }
        public Nullable<decimal> STOCK_MAXI { get; set; }
        public Nullable<decimal> STOCK_MINI { get; set; }
        public Nullable<bool> STOCK_NEG { get; set; }
        public string TIEMPO { get; set; }
        public string TIPO_PROMO { get; set; }
        public string USA_ESC { get; set; }
        public Nullable<bool> USA_PARTID { get; set; }
        public Nullable<bool> USA_SCRAP { get; set; }
        public Nullable<bool> USA_SERIE { get; set; }
        public Nullable<System.DateTime> FECHA_MODI { get; set; }
        public string DISP_MOVIL { get; set; }
        public string RENTA_UM_S { get; set; }
        public string RENTA_UM_V { get; set; }
        public string RENTA_PROD { get; set; }
        public Nullable<decimal> RENTA_EQ_S { get; set; }
        public Nullable<decimal> RENTA_EQ_V { get; set; }
        public Nullable<bool> GENERACOT { get; set; }
        public Nullable<bool> USA_CTRPRE { get; set; }
        public Nullable<short> COD_II_V_2 { get; set; }
        public Nullable<short> COD_SII_V2 { get; set; }
        public Nullable<decimal> IMP_II_V_2 { get; set; }
        public string BASE { get; set; }
        public string VALOR1 { get; set; }
        public string VALOR2 { get; set; }
        public string MET_DES_PA { get; set; }
        public string ORD_DES_PA { get; set; }
        public Nullable<short> AFIP_UM_S { get; set; }
        public Nullable<short> AFIP_UM_V { get; set; }
        public Nullable<decimal> AFIP_EQ_S { get; set; }
        public Nullable<decimal> AFIP_EQ_V { get; set; }
        public Nullable<bool> FERIADOS { get; set; }
        public Nullable<bool> LUNES { get; set; }
        public Nullable<bool> MARTES { get; set; }
        public Nullable<bool> MIERCOLES { get; set; }
        public Nullable<bool> JUEVES { get; set; }
        public Nullable<bool> VIERNES { get; set; }
        public Nullable<bool> SABADO { get; set; }
        public Nullable<bool> DOMINGO { get; set; }
        public string COD_PLANTI { get; set; }
        public Nullable<bool> AFECTA_AF { get; set; }
        public string COD_TIPOB { get; set; }
        public string REMITIBLE { get; set; }
        public string CARGA_RAP { get; set; }
        public string COD_STA11 { get; set; }
        public Nullable<short> AFIP_UMEX_V { get; set; }
        public Nullable<short> AFIP_UMEX_S { get; set; }
        public Nullable<bool> LLEVA_DOBLE_UNIDAD_MEDIDA { get; set; }
        public Nullable<decimal> EQUIVALENCIA_STOCK_2 { get; set; }
        public Nullable<int> ID_MEDIDA_STOCK_2 { get; set; }
        public Nullable<int> ID_MEDIDA_STOCK { get; set; }
        public Nullable<int> ID_MEDIDA_VENTAS { get; set; }
        public Nullable<decimal> EQUIVALENCIA_MEDIDA_PESO { get; set; }
        public Nullable<decimal> EQUIVALENCIA_MEDIDA_VOLUMEN { get; set; }
        public Nullable<System.DateTime> FECHA_INGRESO { get; set; }
        public string USUARIO { get; set; }
        public string TERMINAL { get; set; }
        public Nullable<System.DateTime> FECHA_ULTIMA_MODIFICACION { get; set; }
        public string USUA_ULTIMA_MODIFICACION { get; set; }
        public string TERM_ULTIMA_MODIFICACION { get; set; }
        public string OBSERVACIONES { get; set; }
        public int ID_STA11 { get; set; }
        public Nullable<bool> DESCARGA_NEGATIVO_STOCK { get; set; }
        public Nullable<bool> DESCARGA_NEGATIVO_VENTAS { get; set; }
        public Nullable<int> ID_TYPS { get; set; }
        public Nullable<bool> ADMITE_VENTA_FRACCIONADA { get; set; }
        public Nullable<bool> ADMITE_INVITACION { get; set; }
        public string TIPO_RECARGO_VENTA_FRACCIONADA { get; set; }
        public Nullable<decimal> PORC_RECARGO_VENTA_FRACCIONADA { get; set; }
        public Nullable<decimal> IMP_RECARGO_VENTA_FRACCIONADA { get; set; }
        public string CIGARRILLO { get; set; }
        public string RELACION_UNIDADES_STOCK { get; set; }
        public Nullable<decimal> DESVIO_CONTROL_UNIDADES_STOCK { get; set; }
        public Nullable<decimal> DESVIO_CIERRE_PEDIDOS { get; set; }
        public Nullable<int> ID_MEDIDA_CONTROL_STOCK { get; set; }
        public Nullable<bool> USA_CONTROL_UNIDADES_STOCK { get; set; }
        public string PRODUCTO_TERMINADO_COT { get; set; }
        public string COD_NCM { get; set; }
        public string EDITA_PRECIO { get; set; }
        public string SERIE_DESC_ADICIONAL_1 { get; set; }
        public string SERIE_DESC_ADICIONAL_2 { get; set; }
        public string EGRESO_MODIFICA_PARTIDA_PROPUESTA { get; set; }
        public Nullable<bool> CALCULA_CM { get; set; }
        public string ORIGEN_PARA_CM { get; set; }
        public Nullable<bool> PUBLICA_WEB { get; set; }
        public string PUBLICA_WEB_PEDIDO { get; set; }
        public string SINCRONIZA_WEB_PEDIDO { get; set; }
        public Nullable<bool> DESCRIPCION_VARIABLE { get; set; }
        public Nullable<int> ID_ACTIVIDAD_DGI { get; set; }
        public string DESCRIPCION_LARGA { get; set; }
        public Nullable<int> ID_TRA_CLASE_ARTICULO { get; set; }
        public Nullable<int> ID_CODIGO_ITEM_TURISMO { get; set; }
        public Nullable<int> ID_TIPO_UNIDAD_TURISMO { get; set; }
        public Nullable<int> ID_TIPO_ITEM_AFIP { get; set; }
        public string CA_967_MONEDA { get; set; }
        public string SOLICITA_PRECIO { get; set; }
        public string SEPARADOR_DEFECTO { get; set; }
        public Nullable<bool> REV_PEND { get; set; }
        public string DESCRIPCION_RECETA { get; set; }
        public Nullable<System.DateTime> FECHA_MODI_PORC_UTILIDAD { get; set; }
    }
}
