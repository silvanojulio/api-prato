//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FedericoPrato
{
    using System;
    using System.Collections.Generic;
    
    public partial class GVA21
    {
        public int ID_GVA21 { get; set; }
        public string FILLER { get; set; }
        public string APRUEBA { get; set; }
        public Nullable<short> CIRCUITO { get; set; }
        public string COD_CLIENT { get; set; }
        public string COD_SUCURS { get; set; }
        public string COD_TRANSP { get; set; }
        public string COD_VENDED { get; set; }
        public string COMENTARIO { get; set; }
        public Nullable<bool> COMP_STK { get; set; }
        public Nullable<short> COND_VTA { get; set; }
        public Nullable<decimal> COTIZ { get; set; }
        public short ESTADO { get; set; }
        public Nullable<bool> EXPORTADO { get; set; }
        public Nullable<System.DateTime> FECHA_APRU { get; set; }
        public Nullable<System.DateTime> FECHA_ENTR { get; set; }
        public Nullable<System.DateTime> FECHA_PEDI { get; set; }
        public string HORA_APRUE { get; set; }
        public string ID_EXTERNO { get; set; }
        public string LEYENDA_1 { get; set; }
        public string LEYENDA_2 { get; set; }
        public string LEYENDA_3 { get; set; }
        public string LEYENDA_4 { get; set; }
        public string LEYENDA_5 { get; set; }
        public Nullable<bool> MON_CTE { get; set; }
        public Nullable<short> N_LISTA { get; set; }
        public string N_REMITO { get; set; }
        public string NRO_O_COMP { get; set; }
        public string NRO_PEDIDO { get; set; }
        public Nullable<short> NRO_SUCURS { get; set; }
        public string ORIGEN { get; set; }
        public Nullable<decimal> PORC_DESC { get; set; }
        public string REVISO_FAC { get; set; }
        public string REVISO_PRE { get; set; }
        public string REVISO_STK { get; set; }
        public Nullable<short> TALONARIO { get; set; }
        public short TALON_PED { get; set; }
        public Nullable<decimal> TOTAL_PEDI { get; set; }
        public string TIPO_ASIEN { get; set; }
        public string MOTIVO { get; set; }
        public string HORA { get; set; }
        public string COD_CLASIF { get; set; }
        public Nullable<int> ID_ASIENTO_MODELO_GV { get; set; }
        public Nullable<short> TAL_PE_ORI { get; set; }
        public string NRO_PE_ORI { get; set; }
        public Nullable<System.DateTime> FECHA_INGRESO { get; set; }
        public string HORA_INGRESO { get; set; }
        public string USUARIO_INGRESO { get; set; }
        public string TERMINAL_INGRESO { get; set; }
        public Nullable<System.DateTime> FECHA_ULTIMA_MODIFICACION { get; set; }
        public string HORA_ULTIMA_MODIFICACION { get; set; }
        public string USUA_ULTIMA_MODIFICACION { get; set; }
        public string TERM_ULTIMA_MODIFICACION { get; set; }
        public Nullable<int> ID_DIRECCION_ENTREGA { get; set; }
        public Nullable<bool> ES_PEDIDO_WEB { get; set; }
        public Nullable<int> WEB_ORDER_ID { get; set; }
        public Nullable<System.DateTime> FECHA_O_COMP { get; set; }
        public string ACTIVIDAD_COMPROBANTE_AFIP { get; set; }
        public Nullable<int> ID_ACTIVIDAD_EMPRESA_AFIP { get; set; }
        public Nullable<short> TIPO_DOCUMENTO_PAGADOR { get; set; }
        public string NUMERO_DOCUMENTO_PAGADOR { get; set; }
        public string NRO_OC_COMP { get; set; }
        public string USUARIO_TIENDA { get; set; }
        public string TIENDA { get; set; }
        public string ORDER_ID_TIENDA { get; set; }
        public Nullable<decimal> TOTAL_DESC_TIENDA { get; set; }
        public string TIENDA_QUE_VENDE { get; set; }
        public Nullable<decimal> PORCEN_DESC_TIENDA { get; set; }
        public string USUARIO_TIENDA_VENDEDOR { get; set; }
        public Nullable<int> ID_NEXO_PEDIDOS_ORDEN { get; set; }
    }
}
